import React, {useState} from 'react'
import NavBar from '../components/NavBar'

import Image from 'react-bootstrap/Image'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Card from 'react-bootstrap/Card'
import ListGroup from 'react-bootstrap/ListGroup'
import Tabs from 'react-bootstrap/Tabs'
import Tab from 'react-bootstrap/Tab'

import AppHelper from '../app_helper.js'

import maleAvatar from '../assets/male-avatar.svg'


import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faHome, faHeart, faBirthdayCake, faGlobeAsia } from '@fortawesome/free-solid-svg-icons'


export default function Profile(){

	const [ firstName, setFirstName ] = useState("")
	const [ lastName, setLastName ] = useState("")
	const [ email, setEmail ] = useState("")

	const homeIcon = <FontAwesomeIcon icon={faHome}/>
	const heartIcon = <FontAwesomeIcon icon={faHeart} />
	const birthdayCakeIcon = <FontAwesomeIcon icon={faBirthdayCake} />
	const globeIcon = <FontAwesomeIcon icon={faGlobeAsia} />

	const token = localStorage.getItem('token')
	const options = {

		headers: {

			"Authorization": `Bearer ${token}`
		}

	}

	fetch(`${AppHelper.API_URL}/users/details`, options)
	.then(AppHelper.toJSON)
	.then(data => {

		setFirstName(data.firstName)
		setLastName(data.lastName)
		setEmail(data.email)
		console.log(data)


	})


	return(
			<>
			<NavBar />
			
			<Container className="profileContainer">
				<Image src={maleAvatar} />
				<h2>{`${firstName} ${lastName}`}</h2>
				<p>{`${email}`}</p>
				<Container>
					<Card className="mb-3">
						<Card.Body>
							<Card.Title>About Me:</Card.Title>
							<Card.Text>
								 Lorem ipsum esse mollit commodo excepteur reprehenderit sed tempor esse elit ut duis culpa aliquip laboris laborum aute anim consequat labore est duis est ad fugiat esse ut qui consequat. Quis ullamco dolore ad occaecat occaecat pariatur anim et nisi ea.  
	      					
	      					</Card.Text>
						</Card.Body>
					</Card>
					<Card className="mb-3">
						<Card.Body>
							<Card.Title>Personal Details</Card.Title>
							<Card.Text> 
	      						<Container>
	      							<Row className="mb-3">
	      								<Col>
	      									{globeIcon} Is from the Philippines
	      								</Col>
	      								<Col>
	      									{homeIcon} Lives in Lorem Ipsum
	      								</Col>
	      							</Row>
	      							<Row className="mb-3">
	      								<Col>
	      									{heartIcon} Single
	      								</Col>
	      								<Col>
	      									{birthdayCakeIcon} June 1, 1991
	      								</Col>
	      							</Row>
	      						</Container>
	      					</Card.Text>
						</Card.Body>
					</Card>
					<Card className="mb-3">
						<Card.Body>
							<Card.Text> 
	      						<Container>
	      							<Tabs defaultActiveKey="friends" id="uncontrolled-tab-example">
									  <Tab eventKey="friends" title="Friends">
									    	Lorem ipsum esse mollit commodo excepteur reprehenderit sed tempor esse elit ut duis culpa aliquip laboris laborum aute anim consequat labore est duis est ad fugiat esse ut qui consequat. Quis ullamco dolore ad occaecat occaecat pariatur anim et nisi ea. 
									  </Tab>
									  <Tab eventKey="photos" title="Photos">
									    test
									  </Tab>
									  <Tab eventKey="videos" title="Videos">
									   		Lorem ipsum esse mollit commodo excepteur reprehenderit sed tempor esse elit ut duis culpa aliquip laboris laborum aute anim consequat labore est duis est ad fugiat esse ut qui consequat. Quis ullamco dolore ad occaecat occaecat pariatur anim et nisi ea. 
									  </Tab>
									  <Tab eventKey="article" title="Articles">
									   		Lorem ipsum esse mollit commodo excepteur reprehenderit sed tempor esse elit ut duis culpa aliquip laboris laborum aute anim consequat labore est duis est ad fugiat esse ut qui consequat. Quis ullamco dolore ad occaecat occaecat pariatur anim et nisi ea. 
									  </Tab>
									</Tabs>
	      						</Container>
	      					</Card.Text>
						</Card.Body>
					</Card>
						
				</Container>
			</Container>
			</>
		)


}