import React, { Fragment, useState } from 'react'
import {Redirect} from 'react-router-dom'

import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Image from 'react-bootstrap/Image'
import logo from '../assets/avatar.svg'
import Nav from 'react-bootstrap/Nav'

import AppHelper from '../app_helper.js'
import Swal from 'sweetalert2'
import '../App.css'




const results = require('../results')

export default function Login(){

	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")
	const [willRedirect, setWillRedirect] = useState(false)
	
	
	function validate(e){
		
		e.preventDefault()
		
		const payload = {

			method: "POST",
			headers: {

				"Content-Type": "application/json"

			},
			body: JSON.stringify({

				email: email,
				password: password
			})
		}

		//verify if user is registered
		//if true redirect to profile, else show alert
		fetch(`${AppHelper.API_URL}/users/login`, payload)
		.then(AppHelper.toJSON)
		.then(data => {
			
			console.log(data)

			if(data){
				
				localStorage.setItem('token', data.accessToken)

				Swal.fire({ ...results.result.loginSuccess })
				setWillRedirect(true)
			
			} else {

				Swal.fire({ ...results.result.incorrect })						 
			} 
					

		})
		
	}

	return (
			<Fragment>
				{
					willRedirect === true
					?
						<Redirect to="/profile"/>
					:
						<div className="loginContainer">
							<Form id="loginForm" onSubmit={e => validate(e)}>
								<Image src={logo}/>
								<Form.Group controlId="userEmail">
									<Form.Label>Email</Form.Label>
									<Form.Control type="email" onChange={e => setEmail(e.target.value)} placeholder="Enter Email" required/>
								</Form.Group>

								<Form.Group controlId="password">
									<Form.Label>Password</Form.Label>
									<Form.Control type="password" onChange={e => setPassword(e.target.value)} placeholder="Enter Password" />
								</Form.Group>
								<Button variant="dark" type="submit" block>Login</Button>
								<div className="center-links">
									<Nav.Link href="register">Sign Up</Nav.Link> <Nav.Link href="#login">Forgot password?</Nav.Link> 
								</div>
							</Form>
						</div>
				}
			</Fragment>
	)


}
