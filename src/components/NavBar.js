import React from 'react'
import Nav from 'react-bootstrap/Nav'
import Navbar from 'react-bootstrap/Navbar'
import Form from 'react-bootstrap/Form'
import FormControl from 'react-bootstrap/FormControl'
import Button from 'react-bootstrap/Button'

export default function NavBar(){

	function logout(){

		localStorage.clear()

	}

	return (

			<Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
			  <Navbar.Brand href="#home">Social Media App</Navbar.Brand>
			  <Navbar.Toggle aria-controls="responsive-navbar-nav" />
			  <Navbar.Collapse id="responsive-navbar-nav">
			    <Nav className="mr-auto">
			      <Nav.Link href="/profile">Profile</Nav.Link>
			      <Nav.Link href="/feed">Feed</Nav.Link>
			    </Nav>
			    <Nav>
			      <Nav.Link href="/" onClick={ e => logout() }>Logout</Nav.Link>
			    </Nav>
			  </Navbar.Collapse>
			</Navbar>
		)

}